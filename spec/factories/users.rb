FactoryBot.define do
  factory :user do
    code { Faker::Code.npi }
    name { Gimei.name.kanji.gsub(/[[:blank:]]/, '') }
    password{ 'User_1234' }
    email { Faker::Internet.email }
  end
end