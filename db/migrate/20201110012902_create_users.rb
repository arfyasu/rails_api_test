class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.string :password, null: false

      t.string :email
      t.datetime :last_sign_in_at
      t.timestamps
    end
  end
end
