class User < ApplicationRecord

  validates :code, presence: true
  validates :name, presence: true
  validates :password, presence: true

  def self.search(code)
    return User.all unless code
    User.where(['code LIKE ?', "%#{code}%"])
  end
end
